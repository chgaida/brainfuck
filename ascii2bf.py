#!/usr/bin/env python3


""" 
    Type your name or any other string you want. Returns the ascii string in
    optimized Brainf*ck code (https://en.wikipedia.org/wiki/Brainfuck) 

"""

import math


def gen_bf(a, b, c, sign):
    if a > 0:
        print(">" + ("+" * a) + "[<" + (sign * b) + ">-]<", end="")
    if c > 0:
        print(sign * c, end="")
    print(".", end="")


def asc2bf(usr_str):
    tmp = 0

    for char in usr_str:
        num_char = ord(char)
        if num_char - tmp < 0:
            sign = "-"
            num_char = tmp - num_char
        else:
            sign = "+"
            num_char = num_char - tmp

        a = int(math.sqrt(num_char))
        b = a
        if num_char - a * (b + 1) >= 0:
            b += 1
        c = num_char - a * b
        if num_char < 7:
            a = 0
            c = num_char

        gen_bf(a, b, c, sign)
        tmp = ord(char)


def main():
    usr_str = input("Input: ")
    print("\nBrainf*ck: ")
    asc2bf(usr_str)
    print()


if __name__ == "__main__":
    main()
