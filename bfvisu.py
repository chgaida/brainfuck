#!/usr/bin/env python

import time
import bf_examples as bf


data = [{"loop_cnt": 0, "pointer": 0, "value": 0, "pos": ""}] * 49152

bfck = bf.bfck["b"]


def visualizer(bfck_str, data, data_len):
    col1 = 2
    col2 = 14
    col3 = 20
    outp = ">> "

    # Reset right screen
    screen_right = [[" --- " for k in range(8)] for j in range(8)]

    for i in range(data_len):

        pos = int(data[i]["pos"])
        if bfck_str[pos] == ".":
            outp += chr(data[i]["value"])
        # Reset left screen
        screen_left = [[" " for k in range(24)] for j in range(8)]

        for j in range(8):  # X-Axis
            if data[i]["loop_cnt"] >= 7 - j:
                screen_left[j][col1] = "\u2588"  # 7 -j
            if data[i]["pointer"] >= 7 - j:
                screen_left[j][col2] = "\u2588"  # 7 -j
            if data[i]["pointer"] == 7 - j:
                screen_left[j][col3] = str(data[i]["value"]).zfill(3)
            else:
                screen_left[j][col3] = "   "

            x = divmod(data[i]["pointer"], 8)[0]
            y = divmod(data[i]["pointer"], 8)[1]
            if x == j:
                screen_right[j][y] = " " + str(data[i]["value"]).zfill(3) + " "

        # Print Matrix
        print((" " * 29) + "0    1    2    3    4    5    6    7")  # Header
        for j in range(8):  # Y-Axis
            print(7 - j, end="")
            for k in range(24):
                print(screen_left[j][k], end="")
            for k in range(8):
                print(screen_right[j][k], end="")
            print()

        # Footer
        print(" Loop Count   Reg   Value" + (" " * 41) + "i: ", i)
        print()
        x1 = divmod(data[i]["pos"], 66)[0] * 66
        x2 = x1 + 66
        print(bfck_str[x1:x2])
        print(" " * (pos - x1) + "^" + " " * (68 - pos))
        print()
        print(outp)

        if i < data_len - 1:
            print("\033[F" * 16)  # Move up
        else:
            print()
        time.sleep(0.03)


def main():
    pointer = 0
    memory = [0 for x in range(255)]
    stack = [0 for x in range(255)]
    loop_cnt = 0
    char_pos = 0
    ascii_out = ""
    i = 0

    try:
        while char_pos < len(bfck):

            if bfck[char_pos] == "+":
                if memory[pointer] == 255:
                    memory[pointer] = 0
                else:
                    memory[pointer] += 1
            if bfck[char_pos] == "-":
                if memory[pointer] == 0:
                    memory[pointer] = 255
                else:
                    memory[pointer] -= 1
            if bfck[char_pos] == ">":
                pointer += 1
            if bfck[char_pos] == "<":
                pointer -= 1
            if bfck[char_pos] == "[":
                loop_cnt += 1
                stack[loop_cnt] = char_pos
            if bfck[char_pos] == "]":
                if memory[pointer] == 0:
                    loop_cnt -= 1
                else:
                    char_pos = stack[loop_cnt]
            if bfck[char_pos] == ".":
                ascii_out += chr(memory[pointer])
            if bfck[char_pos] == ",":
                pass

            data[i] = {
                "loop_cnt": loop_cnt,
                "pointer": pointer,
                "value": memory[pointer],
                "pos": char_pos,
            }
            i += 1

            char_pos += 1

    except Exception as e:
        print("Error: ", e)
        print("Dump:\n")
        print(i)

    # print(ascii_out)
    print()
    visualizer(bfck, data, i)


if __name__ == "__main__":
    main()
